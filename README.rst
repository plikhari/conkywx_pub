**conkywx - the conky weather program**

**Please see documentation with the program :)**

**To see the documentation [Help] after installation - at the command prompt in a terminal window **
* type **conkywx** and use option **2** to see the documentation in your browser.

OR

* in a terminal window type **conkywx --help**

**Please see the home page for this repo below - trying to put links here drives me crazier than I am ;-)**

https://bitbucket.org/plikhari/conkywx_pub/wiki/Home